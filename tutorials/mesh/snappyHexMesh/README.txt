For snappyHexMesh tutorials, refer to these cases:

- ../../heatTransfer/chtMultiRegionFoam/snappyMultiRegionHeater
- ../../incompressible/simpleFoam/motorBike
- ../../heatTransfer/buoyantBoussinesqSimpleFoam/iglooWithFridges

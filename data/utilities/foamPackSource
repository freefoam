#!/bin/sh
#-------------------------------------------------------------------------------
#               ______                _     ____          __  __
#              |  ____|             _| |_  / __ \   /\   |  \/  |
#              | |__ _ __ ___  ___ /     \| |  | | /  \  | \  / |
#              |  __| '__/ _ \/ _ ( (| |) ) |  | |/ /\ \ | |\/| |
#              | |  | | |  __/  __/\_   _/| |__| / ____ \| |  | |
#              |_|  |_|  \___|\___|  |_|   \____/_/    \_\_|  |_|
#
#                   FreeFOAM: The Cross-Platform CFD Toolkit
#
# Copyright (C) 2008-2012 Michael Wild <themiwi@users.sf.net>
#                         Gerber van der Graaf <gerber_graaf@users.sf.net>
#-------------------------------------------------------------------------------
# License
#   This file is part of FreeFOAM.
#
#   FreeFOAM is free software: you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   FreeFOAM is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with FreeFOAM.  If not, see <http://www.gnu.org/licenses/>.
#
# Script
#     foamPackSource [-h|--help]
#                    [-c|--create-tag [-t|--tag-key <keyId>] [-m|--tag-msg <msg>]]
#                    [-s|--sign-key <keyId>] <version> <outputDir>
#
# Description
#     Creates a source tar-ball for a given <version> in <outputDir>.
#
#     The <version> argument requires that a git tree-ish object with the name
#     v<version> exists (i.e. a tag named e.g. v0.1.0).  If no such tag exists,
#     you can specify -c and the current HEAD will be tagged as v<version>. The
#     output is named <outputDir>/freefoam-<version>.tar.bz2 and a detached
#     signature is created using the users default GPG-key in
#     <outputDir>/freefoam-<version>.tar.bz2.sig (can be overriden using -s).
#     Further the md5 and sha1 digests are computed and placed in
#     <outputDir>/freefoam-<version>.tar.bz2.md5 and
#     <outputDir>/freefoam-<version>.tar.bz2.sha1, respectively.
#
#     -h
#     --help             Print a help message
#     -c
#     --create-tag       Create a tag named v<version>. By default it is signed
#                        by the default e-mail address's GPG-key.
#     -t <keyId>
#     --tag-key <keyId>  If -c is used, specify a different GPG-key ID to sign.
#     -m <msg>
#     --tag-msg <msg>    Instead of prompting, use the given tag-message when
#                        -c is specified.
#     -s <keyId>
#     --sign-key <keyId> Sign the created tar-ball using the GPG-key <keyId>
#                        instead of using the default key.
#     -u
#     --upload           Upload to sourceforge.net.
#     <version>          Specify the version for which to create the source
#                        tar-ball.  This expects a git object with the name
#                        v<version> to exist, which should be a tag.
#     <outputDir>        The directory in which to place the tar-ball.
#
#------------------------------------------------------------------------------

SUBDIRECTORY_OK=Yes

OPTIONS_SPEC="\
foamPackSource [options] <version> <outputDir>

Creates a source tar-ball for a given <version> in <outputDir>.

The <version> argument requires that a git tree-ish object with the name
v<version> exists (i.e. a tag named e.g. v0.1.0).  If no such tag exists, you
can specify -c and the current HEAD will be tagged as v<version>. The output
is named <outputDir>/freefoam-<version>.tar.bz2 and a detached signature is
created using the users default GPG-key in
<outputDir>/freefoam-<version>.tar.bz2.sig (can be overriden using -s).
Further the md5 and sha1 digests are computed and placed in
<outputDir>/freefoam-<version>.tar.bz2.md5 and
<outputDir>/freefoam-<version>.tar.bz2.sha1, respectively.
--
c,create-tag Create a tag named v<version> and sign it with the default GPG-key.
t,tag-key=   If -c is used, specify a different GPG-key ID to sign.
m,tag-msg=   Use the given tag-message if -c is specified instead of prompting.
s,sign-key=  Sign the tar-ball with the specified, non-default GPG-key.
u,upload     Upload the created tar-ball to sourceforge.net."

. "$(git --exec-path)/git-sh-setup"

cd_to_toplevel

if [ $# -lt 2 ]; then
   die "Require version number and output directory arguments."
fi

CREATE_TAG=no
CUSTOM_TAG_KEY=
TAG_MSG=
SIGN_KEY=
DO_UPLOAD=no
VERSION=
OUTPUT_DIR=

while [ $# -gt 0 ]; do
  case "$1" in
     -c|--create-tag)
        CREATE_TAG=yes
        shift
        ;;
     -t|--tag-key)
        CUSTOM_TAG_KEY="$2"
        shift 2
        ;;
     -m|--tag-msg)
        TAG_MSG="$2"
        shift 2
        ;;
     -s|--sign-key)
        SIGN_KEY="--default-key $2"
        shift 2
        ;;
     -u|--upload)
        DO_UPLOAD=yes
        shift
        ;;
     --)
        VERSION="$2"
        OUTPUT_DIR="$3"
        shift 3
        break
        ;;
  esac
done

if [ -z "$VERSION" -o -z "$OUTPUT_DIR" ]; then
   die "Version and output directory arguments are required"
fi

if ! [ -d "$OUTPUT_DIR" -a -w "$OUTPUT_DIR" ]; then
   die "The output directory '$OUTPUT_DIR' does not exist or is not writeable"
fi

for ext in "" .sig .md5 .sha1; do
   if [ -e "$OUTPUT_DIR/freefoam-$VERSION.tar.bz2$ext" ]; then
      die "The output file $OUTPUT_DIR/freefoam-$VERSION.tar.bz2$ext already exists" >&2
   fi
done

# create the tag if requested
if [ "$CREATE_TAG" = "yes" ]; then
   if git rev-list --max-count=1 --quiet --tags "v$VERSION" > /dev/null 2>&1; then
      die "Cannot create tag v$VERSION, it already exists"
   fi
   GIT_TAG_OPTS=
   if [ -n "$CUSTOM_TAG_KEY" ]; then
      GIT_TAG_OPTS="$GIT_TAG_OPTS -u $CUSTOM_TAG_KEY"
   else
      GIT_TAG_OPTS="$GIT_TAG_OPTS -s"
   fi
   if [ -n "$TAG_MSG" ]; then
      GIT_TAG_OPTS="$GIT_TAG_OPTS -m '$TAG_MSG'"
   fi
   echo "Creating tag 'v$VERSION'"
   eval "git tag $GIT_TAG_OPTS v$VERSION"
fi

# create the tar-ball
echo "Creating the tar ball"
TMP=`mktemp -d -t freefoam-XXXX` || die "Failed to create temporary directory"
git archive --format=tar --prefix=freefoam-$VERSION/ v$VERSION > \
  $TMP/freefoam-$VERSION.tar || die "Failed to create tar file"

# create HTML docs and .build.cmake and add to tar-ball
echo "Creating HTML docs and adding them to the tar-ball"
mkdir $TMP/freefoam-$VERSION
for f in ChangeLog INSTALL README ReleaseNotes HACKING; do
   git show v$VERSION -- $f | \
      asciidoc -a toc -f data/asciidoc/html.conf - > \
      $TMP/freefoam-$VERSION/$f.html
done
echo "set(FOAM_BUILD_NUMBER `git log -1 --pretty=format:%h`)" > \
   $TMP/freefoam-$VERSION/.build.cmake
tar -C $TMP -rf $TMP/freefoam-$VERSION.tar freefoam-$VERSION

echo "Compressing the tar ball"
bzip2 --best $TMP/freefoam-$VERSION.tar || die "Failed to compress the tar ball"
mv $TMP/freefoam-$VERSION.tar.bz2 $OUTPUT_DIR/freefoam-$VERSION.tar.bz2 || \
  die "Failed to place the compressed tar ball in '$OUTPUT_DIR'"
rm -rf $TMP || die "Failed to remove the temporary directory '$TMP'"

# sign the tar-ball
echo "Signing the tar ball"
gpg --armor -o $OUTPUT_DIR/freefoam-$VERSION.tar.bz2.sig $SIGN_KEY \
  --sign --detach-sign $OUTPUT_DIR/freefoam-$VERSION.tar.bz2 || \
  die "Failed to sign the tar ball"

# create md5 and sha1 sums
echo "Computing checksums of the tar ball"
(cd $OUTPUT_DIR; openssl md5 -out freefoam-$VERSION.tar.bz2.md5 freefoam-$VERSION.tar.bz2)
(cd $OUTPUT_DIR; openssl sha1 -out freefoam-$VERSION.tar.bz2.sha1 freefoam-$VERSION.tar.bz2)

# create a MarkDown version of the ReleaseNotes
asciidoc -b docbook -f data/asciidoc/chunked.conf -o - ReleaseNotes | \
   xsltproc data/utilities/db2md.xsl - > $OUTPUT_DIR/README.md

# upload to sf.net
if [ "$DO_UPLOAD" = "yes" ]; then
   echo "Uploading files to sourceforge.net"
   rsync -avP $OUTPUT_DIR/freefoam-$VERSION.tar.bz2 \
              $OUTPUT_DIR/freefoam-$VERSION.tar.bz2.sig \
              $OUTPUT_DIR/freefoam-$VERSION.tar.bz2.md5 \
              $OUTPUT_DIR/freefoam-$VERSION.tar.bz2.sha1 \
              $OUTPUT_DIR/README.md \
              frs.sourceforge.net:/home/frs/project/f/fr/freefoam/FreeFOAM/$VERSION/ || \
              die "Failed to upload the files to sourceforge.net"
  echo "Finished upload."
fi

echo "Done"

# ------------------------- vim: set sw=3 sts=3 et: --------------- end-of-file

#!@CMAKE_COMMAND@ -P
#-------------------------------------------------------------------------------
#               ______                _     ____          __  __
#              |  ____|             _| |_  / __ \   /\   |  \/  |
#              | |__ _ __ ___  ___ /     \| |  | | /  \  | \  / |
#              |  __| '__/ _ \/ _ ( (| |) ) |  | |/ /\ \ | |\/| |
#              | |  | | |  __/  __/\_   _/| |__| / ____ \| |  | |
#              |_|  |_|  \___|\___|  |_|   \____/_/    \_\_|  |_|
#
#                   FreeFOAM: The Cross-Platform CFD Toolkit
#
# Copyright (C) 2008-2012 Michael Wild <themiwi@users.sf.net>
#                         Gerber van der Graaf <gerber_graaf@users.sf.net>
#-------------------------------------------------------------------------------
# License
#   This file is part of FreeFOAM.
#
#   FreeFOAM is free software: you can redistribute it and/or modify it
#   under the terms of the GNU General Public License as published by the
#   Free Software Foundation, either version 3 of the License, or (at your
#   option) any later version.
#
#   FreeFOAM is distributed in the hope that it will be useful, but WITHOUT
#   ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
#   FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
#   for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with FreeFOAM.  If not, see <http://www.gnu.org/licenses/>.
#
# Script
#   wmakeToCMake
#
# Description
#   Simple utility to convert a wmake based build system to CMake.
#
#------------------------------------------------------------------------------

# safe-guard against overwriting
if(EXISTS "CMakeLists.txt")
  message(FATAL_ERROR "CMakeLists.txt already exists")
endif()

# get the tools to do the heavy lifting
include("@FOAM_CMAKECONFIG_DIR@/@PROJECT_NAME@WmakeCompatibility.cmake")

# parse Make/files and Make/options
_foam_awt_parse(type target absfiles libs incdirs
  "${CMAKE_CURRENT_SOURCE_DIR}")

# turn file paths into relative paths
set(files)
foreach(f ${absfiles})
  file(RELATIVE_PATH f "${CMAKE_CURRENT_SOURCE_DIR}" "${f}")
  list(APPEND files "${f}")
endforeach()

# transform into nicely formatted lists
string(REPLACE ";" "\n  " files "${files}")
string(REPLACE ";" "\n  " libs "${libs}")

# prepare the call to foam_add_(executable|library)
if(type STREQUAL EXE)
  set(type executable)
else()
  set(type library)
endif()

# write the CMakeLists.txt
file(WRITE "${CMAKE_CURRENT_SOURCE_DIR}/CMakeLists.txt" "
cmake_minimum_required(VERSION 2.8)
project(${target})

find_package(FreeFOAM REQUIRED)
include("\${FOAM_USE_FILE}")

set_source_files_properties(
  Make/files
  Make/options
  PROPERTIES HEADER_FILE_ONLY TRUE
  )

foam_add_${type}(\${PROJECT_NAME}
  ${files}
  )

target_link_libraries(\${PROJECT_NAME}
  ${libs}
  )
")

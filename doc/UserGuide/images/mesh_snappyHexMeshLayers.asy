import mesh_snappyHexMeshCommon;

add(domain);
draw(basegrid);
draw(splitgrid);
draw(castel);
add(warpedcells);
add(addedlayers);
filldraw(snappedcar, white, black);

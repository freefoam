// process with:
//   asy -f <fmt> -tex <pdflatex|xelatex> initial
// DO NOT SPECIFY AN OUTPUT NAME (this confuses asy...)

import tut_damBreak_commonDamBreak;

drawDamBreak("tut_damBreak_initial_field.png", "tut_damBreak_bar.png");

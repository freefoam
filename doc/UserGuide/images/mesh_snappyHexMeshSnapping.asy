import mesh_snappyHexMeshCommon;

add(domain);
draw(basegrid);
draw(splitgrid);
draw(castel);
add(warpedcells);
filldraw(snappedcar, white, black);

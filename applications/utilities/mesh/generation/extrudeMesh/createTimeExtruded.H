    Info<< "Create time\n" << endl;

    Time runTimeExtruded
    (
        Time::controlDictName,
        args.rootPath(),
        args.caseName()
    );

// ************************ vim: set sw=4 sts=4 et: ************************ //
